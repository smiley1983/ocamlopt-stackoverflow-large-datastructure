The genbug.ml program writes code which, when compiled, generates a stack
overflow in ocamlopt.

The script named demonstrate.sh shows the steps to reproduce the issue.
