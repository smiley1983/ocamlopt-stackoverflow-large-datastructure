(* Genbug - code to generate code which will trigger a stack overflow when
 * ocamlopt attempts to compile it. 
 * With 150000 elements, it always overflows on the test system.
 * With 130440 elements, it sometimes overflows and sometimes doesn't.
 * With 200000 elements, building bytecode also results in a stack overflow.
 *
 * Tested on AMD FX8320 with 8GB RAM, 43% used. RAM usage increases by 
 * about 3 or 4 percent by the time the overflow occurs (no proper profiling
 * has been done, this is just from watching the stats in top).
 *
 * Also tested on Intel Atom N450 with 2GB RAM, 23% used. RAM usage rose
 * and fell by about 23% (to a limit of about 48%) until overflow. The same
 * inconsistent behaviour at around 130440 elements was observed on this
 * machine as well.
 *
 * OCaml version on both systems: 4.02.3
 * OS on both systems: Arch linux 64-bit
 * 
 *)

type 'a d = [ `L | `N of 'a * 'a d];; 

let rec gen_struct count v =
  if count = 0 then
    `L
  else
    `N (v, gen_struct (count - 1) v)
;;

let rec print_node = function
| `L -> print_string "`L"
| `N (v, n) -> 
  print_string ("`N (\"" ^ v ^ "\"),");
  print_node n
;;

let print_struct d =
  print_string "let ds  = ";
  print_node d;
  print_string ";;"
;;

let _ =
  print_string ("type 'a d = [ `L | `N of 'a * 'a d];;\n");
  print_struct (gen_struct 150000 "a") 
(*  print_struct (gen_struct 130440 "a") *)
;;

