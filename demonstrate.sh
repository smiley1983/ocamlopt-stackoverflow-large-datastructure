#!/bin/bash
echo "this may take a while..."
ocamlbuild genbug.native
./genbug.native > bug.ml
echo "genbug took $SECONDS seconds to run."
ocamlbuild bug.byte
echo "bug.byte built after $SECONDS seconds."
ocamlbuild bug.native
echo "This script took $SECONDS seconds to run."
